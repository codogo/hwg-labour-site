import styled from "styled-components";

import { Image, } from "semantic-ui-react";

// ----------------------------

export const TopImage = styled(Image)`
	width: 100%;
	height: 400px;
	object-fit: cover;
	z-index: -1;
`;
